﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZoneScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //if ballscript exists in the object, the object will die
        BallScript ballScript = other.GetComponent<BallScript>();
        if(ballScript == true)
        {
            //call die script in BallScript
            ballScript.Die();
        }
    }
}
