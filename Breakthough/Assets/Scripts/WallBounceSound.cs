﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBounceSound : MonoBehaviour
{
    public AudioClip collideSound;
    private AudioSource collideSource;
    // Start is called before the first frame update
    void Start()
    {
        if (collideSound != null)
        {
            collideSource = gameObject.AddComponent<AudioSource>();
            collideSource.clip = collideSound;
            collideSource.loop = false;
            collideSource.playOnAwake = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

        //void PlayCollideSound(GameObject col)
        //{
        //    if (collideSound != null)
         //   {
         //       collideSource.Play();
         //   }
        //}
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        foreach (ContactPoint2D contact in col.contacts)
        {
            if (col.transform.tag == "Ball")
            {
                collideSource.Play();

            }
        }
    }
}
