﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerUpSelfDestroy : MonoBehaviour
{
    // Start is called before the first frame update
    public string powerUpName;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player1" || col.gameObject.tag == "Player2" || col.gameObject.name == "DeadZone")
        {
            Destroy(gameObject);
            //add sound/sfx here to show that its picked up
        }
    }
}
