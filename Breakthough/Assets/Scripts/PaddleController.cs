﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    private GameControllerScript GCS;


    public float paddleSpeed;
    public KeyCode player1MoveR;
    public KeyCode player1MoveL;
    public KeyCode player2MoveR;
    public KeyCode player2MoveL;

    public Animator animator;

    public GameObject ballPrefab;

    public Transform TheCanvas;

    public GameObject newBall = null;

    public float ballForce = 300f;

    public AudioClip hitSound;
    private AudioSource hitSource;

    public AudioClip collideSound;
    private AudioSource collideSource;

    //stun timer
    public float stunTime = 0;
    public float maxStunTime = 2;

    //jump timer
    public float player1_JumpTime = 0;
    public float player2_JumpTime = 0;
    public float player1_MaxJumpTime = 1;
    public float player2_MaxJumpTime = 1;

    //Size powerup
    public float originalPos1 = 0;
    public float originalPos2 = 0;
    public float sizePowerUpTime = 0;
    public Vector3 originalSize;

    //smallsize powerup
    public float smallSizePowerUpTime = 0;

    //multiball powerup

    private bool MultiBall;
    
    //gun powerup
    public float gunPowerUpTime = 0;
    public float fireCooldown = 0.5f;
    public float fireCooldownLeft = 0.5f;

    public GameObject leftMuzzle;
    public GameObject rightMuzzle;
    public GameObject bullet = null;
    public GameObject bulletPrefab;
    //gun sound
    public AudioClip gunSound;
    private AudioSource gunSource;
    //coin sound
    public AudioClip coinSound;
    private AudioSource coinSource;
    // Start is called before the first frame update
    void Start()
    {
        GCS = GameObject.Find("GameController").GetComponent<GameControllerScript>();
        SpawnBall();
        if (hitSound != null)
        {
            hitSource = gameObject.AddComponent<AudioSource>();
            hitSource.clip = hitSound;
            hitSource.loop = false;
            hitSource.playOnAwake = false;
        }

        if (collideSound != null)
        {
            collideSource = gameObject.AddComponent<AudioSource>();
            collideSource.clip = collideSound;
            collideSource.loop = false;
            collideSource.playOnAwake = false;
        }
        if (gameObject.tag == "Player1")
        {
            originalPos1 = transform.position.y;
        }
        if (gameObject.tag == "Player2")
        {
            originalPos2 = transform.position.y;
        }
        originalSize = GetComponent<Transform>().localScale;

        if (gunSound != null)
        {
            gunSource = gameObject.AddComponent<AudioSource>();
            gunSource.clip = gunSound;
            gunSource.loop = false;
            gunSource.playOnAwake = false;
        }

        if (coinSound != null)
        {
            coinSource = gameObject.AddComponent<AudioSource>();
            coinSource.clip = coinSound;
            coinSource.loop = false;
            coinSource.playOnAwake = false;
        }
    }
    public void SpawnBall()
    {
        if(GCS.CurrentBall == 0 || MultiBall == true)
        {
            if (ballPrefab == null)
            {
                //Show the message in debug if ball prefab is not added
                Debug.Log("add ball prefab!");
                return;
            }
            //transform.position = position of paddle, use Vector3 since you can't just do transform.position.y + 0.6f
            Vector3 ballPosition = transform.position + new Vector3(0, 0.6f, 0);
            newBall = Instantiate(ballPrefab, ballPosition, Quaternion.identity) as GameObject;
            //put the new ball within the Canvas so it doens't scale like crazy
            newBall.transform.SetParent(TheCanvas);
            newBall.transform.localScale = new Vector3(40f, 40f, 0);
            
            if(MultiBall == false)
            {
                GCS.playerLives--;
            }
            MultiBall = false;

        }
        
    }
    private void StunPlayer()
    {
        stunTime = maxStunTime;
        PlayCollideSound();
    }

    private void jumpTimePlayer1()
    {
        player1_JumpTime = player1_MaxJumpTime;
    }
    private void jumpTimePlayer2()
    {
        player2_JumpTime = player2_MaxJumpTime;
    }
    // Update is called once per frame
    void Update() 
    {
        //stun animation
        animator.SetFloat("Stun Time", stunTime);
        //stuntime countdown
        if (stunTime > 0)
        {
            stunTime -= Time.deltaTime;
        }
        else
        {
            if (gameObject.tag == "Player1")
            {
                //move left and right using the key variable.
                if (Input.GetKey(player1MoveL))
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(-paddleSpeed, 0);
                }
                if (Input.GetKey(player1MoveR))
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(paddleSpeed, 0);
                }
                if ((!Input.GetKey(player1MoveL)) && (!Input.GetKey(player1MoveR)))
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                }
                if (player1_JumpTime <= 0)
                {
                    if (Input.GetButtonDown("Player1_Jump"))
                    {
                        Vector3 newPos = new Vector3(transform.position.x, transform.position.y + 0.8f);
                        transform.position = newPos;
                        jumpTimePlayer1();

                    }
                    if (player1_JumpTime <= 0)
                        transform.position = new Vector3(transform.position.x, originalPos1);
                }
                if (player1_JumpTime >= 0)
                {
                    player1_JumpTime -= Time.deltaTime;
                }

            }
            if (gameObject.tag == "Player2")
            {
                if (Input.GetKey(player2MoveL))
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(-paddleSpeed, 0);
                }
                if (Input.GetKey(player2MoveR))
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(paddleSpeed, 0);
                }
                if ((!Input.GetKey(player2MoveL)) && (!Input.GetKey(player2MoveR)))
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                }
                if (player2_JumpTime <= 0)
                {
                    if (Input.GetButtonDown("Player2_Jump"))
                    {
                        Vector3 newPos = new Vector3(transform.position.x, transform.position.y + 0.8f);
                        transform.position = newPos;
                        jumpTimePlayer2();

                    }
                    if (player2_JumpTime <= 0)
                        transform.position = new Vector3(transform.position.x, originalPos2);
                }
                if (player2_JumpTime >= 0)
                {
                    player2_JumpTime -= Time.deltaTime;
                }
            }
        }

        if (sizePowerUpTime > 0)
        {
            GetComponent<Transform>().localScale = originalSize + new Vector3(1, 0, 0);
            sizePowerUpTime -= Time.deltaTime;
        }
        if (sizePowerUpTime <=0 && smallSizePowerUpTime <= 0)
        {
            GetComponent<Transform>().localScale = originalSize;
        }
        if (smallSizePowerUpTime > 0)
        {
            GetComponent<Transform>().localScale = originalSize - new Vector3(0.5f, 0, 0);
            smallSizePowerUpTime -= Time.deltaTime;
        }
        if (smallSizePowerUpTime <= 0 && sizePowerUpTime <= 0)
        {
            GetComponent<Transform>().localScale = originalSize;
        }
        //check if newball exists
        if (newBall)
        {
            //make the ball stick to the paddle 
            Rigidbody2D ballRig = newBall.GetComponent<Rigidbody2D>();
            ballRig.position = transform.position + new Vector3(0, 0.6f, 0);
            //Give ball force when pressing Spacebar (Start of the game)
            if (Input.GetButtonDown("Jump"))
            {
                ballRig.AddForce(new Vector2(0, ballForce));
                newBall = null;
            }
;
        }
        if(gunPowerUpTime > 0)
        {
            gunPowerUpTime -= Time.deltaTime;
            fireCooldownLeft -= Time.deltaTime;

            if(fireCooldownLeft <= 0)
            {
                //reset firecooldown to 0
                fireCooldownLeft = fireCooldown;
                shoot();
                gunSource.Play();
            }
        }
        if(gunPowerUpTime <= 0)
        {
            //just in case again
            leftMuzzle.SetActive(false);
            rightMuzzle.SetActive(false);
        }
    }

    private void shoot()
    {
        //just in case 
        leftMuzzle.SetActive(false);
        rightMuzzle.SetActive(false);
        //activate muzzle particle
        leftMuzzle.SetActive(true);
        rightMuzzle.SetActive(true);
        spawnBullet(leftMuzzle);
        spawnBullet(rightMuzzle);
    }
    private void spawnBullet(GameObject muzzle)
    {
        Vector3 spawnPosition = new Vector3(muzzle.transform.position.x, muzzle.transform.position.y + 0.2f, muzzle.transform.position.z);
        bullet = Instantiate(bulletPrefab, spawnPosition, Quaternion.identity);
        Rigidbody2D bulletRB = bullet.GetComponent<Rigidbody2D>();
        bulletRB.AddForce(new Vector2(0, 450f));
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        foreach (ContactPoint2D contact in col.contacts)
        {
            //this make sure that what everside the ball hit e.g. if it hits left side the ball goes left (well doesn't go left but adds X force of goind left by 300f * calc)
            if(col.transform.tag == "Ball")
            {
                //Saves X and Y coordinate in hitpoint to use in calculation
                PlayWallBounceSound(col.gameObject);
                Vector2 hitpoint = contact.point;
                float calc = hitpoint.x - transform.position.x;
                contact.collider.GetComponent<Rigidbody2D>().AddForce(new Vector2(ballForce * calc, 0));

            }
            
        }
        if(stunTime <= 0)
        {
            if (col.gameObject.tag == "Player1" || col.gameObject.tag == "Player2")
            {
                StunPlayer();
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                Debug.Log("IT WORKS");
            }
        }

    }
    void SizePowerUpTime()
    {
        //so the size switches
        smallSizePowerUpTime = 0;
        sizePowerUpTime = 5;
    }

    void GunPowerupTime()
    {
        gunPowerUpTime = 5;
    }

    void SmallSizePowerUpTime()
    {
        //so the size switches
        sizePowerUpTime = 0;
        smallSizePowerUpTime = 5;
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        powerUpSelfDestroy powerup = col.gameObject.GetComponent<powerUpSelfDestroy>();
        if (powerup != null)
        {
            if (powerup.powerUpName == "Size")
            {
                SizePowerUpTime();
                Debug.Log("Size Collected");
            }
            if (powerup.powerUpName == "Lives")
            {
                GCS.playerLives++;
                Debug.Log("Lives Collected");
            }
            if (powerup.powerUpName == "Coin")
            {
                GCS.UpdateScore();
                PlayCoinSound();
            }
            if (powerup.powerUpName == "MultiBall")
            {
                MultiBall = true;
                if(newBall == null)
                {
                    SpawnBall();
                }
                Debug.Log("Multiball Collected");
            }
            if (powerup.powerUpName == "Gun")
            {
                GunPowerupTime();
                Debug.Log("Gun Collected");
            }
            if (powerup.powerUpName == "smallSize")
            {
                SmallSizePowerUpTime();
                Debug.Log("SmallSize Collected");
            }
        }
    }
    void PlayWallBounceSound(GameObject col)
    {
        if (hitSound != null)
        {
            hitSource.Play();
        }
    }
    void PlayCollideSound()
    {
        if (collideSound != null)
        {
            collideSource.Play();
        }
    }
    void PlayCoinSound()
    {
        if (coinSound != null)
        {
            coinSource.Play();
        }
    }
}
