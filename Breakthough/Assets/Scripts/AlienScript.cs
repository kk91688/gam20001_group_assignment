﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienScript : MonoBehaviour
{
    private GameControllerScript GCS;
    public int hitpoints = 1;
    private Transform enemyHolder;
    public float speed;
    public int whichPowerUps;
    public GameObject newPowerup = null;
    public Transform TheCanvas;
    public GameObject Coin;
    public Transform explosion;
    public AudioClip alienDestroy;
    // Start is called before the first frame update
    void Start()
    {
        GCS = GameObject.Find("GameController").GetComponent<GameControllerScript>();
        GCS.CurrentAlien++;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (hitpoints >= 1)
        {
            GCS.UpdateScore();
            hitpoints--;
        }
        if (hitpoints <= 0)
        {
            //random number generator, so 1 in 4 chance of the coin dropping (1, 5) is only 1-4
            whichPowerUps = Random.Range(1, 5);
            if (whichPowerUps == 1)
            {
                Vector3 BrickPosition = transform.position;
                newPowerup = Instantiate(Coin, BrickPosition, Quaternion.identity) as GameObject;
                newPowerup.transform.SetParent(TheCanvas);
                newPowerup.transform.localScale = new Vector3(1f, 1f, 0);
            }
            AudioSource.PlayClipAtPoint(alienDestroy, transform.position);
            Destroy(gameObject);
            Instantiate(explosion, transform.position, explosion.rotation);
            GCS.UpdateScore();
            GCS.CurrentAlien--;
        }
    }

}
