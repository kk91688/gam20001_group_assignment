﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAlienScript : MonoBehaviour
{
    // Start is called before the first frame update
    private GameControllerScript GCS;
    public int hitpoints = 1;
    private Transform enemyHolder;
    public float speed;
    public GameObject LivespowerUp;
    public GameObject Multiball;
    public GameObject powerupSizeObj;
    public GameObject GunPowerUp;
    public GameObject powerUpSmallSizeObj;

    public int whichPowerUps;
    public GameObject newPowerup = null;
    public Transform TheCanvas;

    public Transform explosion;
    public AudioClip alienDestroy;
    void Start()
    {
        GCS = GameObject.Find("GameController").GetComponent<GameControllerScript>();
        GCS.CurrentAlien++;

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (hitpoints >= 1)
        {
            GCS.UpdateScore();
            hitpoints--;
        }

        if (hitpoints <= 0)
        {
            //random number generater to pick which powerups
            whichPowerUps = Random.Range(1, 6);
            GCS.UpdateScore();
            GCS.CurrentAlien--; 
            if(whichPowerUps == 1)
            {
                Vector3 BrickPosition = transform.position;
                newPowerup = Instantiate(powerupSizeObj, BrickPosition, Quaternion.identity) as GameObject;
                newPowerup.transform.SetParent(TheCanvas);
                newPowerup.transform.localScale = new Vector3(1f, 1f, 0);
            }
            if (whichPowerUps == 2)
            {
                Vector3 BrickPosition = transform.position;
                newPowerup = Instantiate(Multiball, BrickPosition, Quaternion.identity) as GameObject;
                newPowerup.transform.SetParent(TheCanvas);
                newPowerup.transform.localScale = new Vector3(1f, 1f, 0);
            }
            if (whichPowerUps == 3)
            {
                Vector3 BrickPosition = transform.position;
                newPowerup = Instantiate(LivespowerUp, BrickPosition, Quaternion.identity) as GameObject;
                newPowerup.transform.SetParent(TheCanvas);
                newPowerup.transform.localScale = new Vector3(1f, 1f, 0);
            }
            if (whichPowerUps == 4)
            {
                Vector3 BrickPosition = transform.position;
                newPowerup = Instantiate(GunPowerUp, BrickPosition, Quaternion.identity) as GameObject;
                newPowerup.transform.SetParent(TheCanvas);
                newPowerup.transform.localScale = new Vector3(1f, 1f, 0);
            }
            if (whichPowerUps == 5)
            {
                Vector3 BrickPosition = transform.position;
                newPowerup = Instantiate(powerUpSmallSizeObj, BrickPosition, Quaternion.identity) as GameObject;
                newPowerup.transform.SetParent(TheCanvas);
                newPowerup.transform.localScale = new Vector3(1f, 1f, 0);
            }
            AudioSource.PlayClipAtPoint(alienDestroy, transform.position);
            Instantiate(explosion, transform.position, explosion.rotation);
            Destroy(gameObject);
        }

    }

}
