﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControllerScript : MonoBehaviour
{
    public int CurrentAlien = 0;
    public int CurrentBall = 0;
    public int maxLives = 4;
    public int playerLives;
    private int score = 0;
    public Text lifeText;
    private string lifeOutput = "Lives - ";

    public Text scoreText;
    private string scoreOutput = "Score - ";

    public GameObject gameOverText;
    public string gameOverScene;


    public Transform enemyHolder;

    public AudioClip winSound;
    private AudioSource winSource;
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("score", 0);
        playerLives = maxLives;
        lifeText.text = lifeOutput + playerLives;
        scoreText.text = scoreOutput + score;

        if (winSound != null)
        {
            winSource = gameObject.AddComponent<AudioSource>();
            winSource.clip = winSound;
            winSource.loop = false;
            winSource.playOnAwake = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        lifeText.text = lifeOutput + playerLives;
        if (playerLives <= 0)
        {
            GameOver(gameOverScene);
        }
        if (enemyHolder.childCount == 0)
        {
            Win("WinScene");
            PlayWinSound();
        }
    }
    public void UpdateScore()
    {
        score += 10;
        scoreText.text = scoreOutput + score;
    }
    void GameOver(string levelName)
    {
        PlayerPrefs.SetInt("score", score);
        SceneManager.LoadScene(levelName);
    }

    void Win(string levelName)
    {
        PlayerPrefs.SetInt("score", score);
        SceneManager.LoadScene(levelName);
    }

    void PlayWinSound()
    {
        if (winSound != null)
        {
            winSource.Play();
        }
    }
}
