﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    private GameControllerScript GCS;
    // Start is called before the first frame update
    void Start()
    {
        //counts the amount of ball
        GCS = GameObject.Find("GameController").GetComponent<GameControllerScript>();
        GCS.CurrentBall++;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Die()
    {
        //Destory the ball and respawn a new ball
        Destroy(gameObject);
        GCS.CurrentBall--;
        //StopGap Measure to stop the lives from going down by 2
        GameObject paddle1 = GameObject.Find("Paddle_1");
        PaddleController paddleScript1 = paddle1.GetComponent<PaddleController>();
        GameObject paddle2 = GameObject.Find("Paddle_2");
        PaddleController paddleScript2 = paddle2.GetComponent<PaddleController>();
        paddleScript1.SpawnBall();
        paddleScript2.SpawnBall();
    }
}
