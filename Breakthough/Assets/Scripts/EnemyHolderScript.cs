﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EnemyHolderScript : MonoBehaviour
{
    private GameControllerScript GCS;
    private Transform enemyHolder;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        GCS = GameObject.Find("GameController").GetComponent<GameControllerScript>();
        //invoke moveAlien again and again
        InvokeRepeating("moveAlien", 0.1f, 0.3f);
        enemyHolder = GetComponent<Transform>();
        moveAlien();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void moveAlien()
    {
        //move the enemyHolder gameobject to the right
        enemyHolder.position += Vector3.right * speed;
        foreach (Transform alien in enemyHolder)
        {
            if (alien.position.x < -9 || alien.position.x > 10)
            {
                speed = -speed;
                enemyHolder.position += Vector3.down * 0.5f;
                return;
            }
            if (alien.position.y <= -3)
            {
                GCS.playerLives = 0;
            }
        }
        if (enemyHolder.childCount == 1)
        {
            CancelInvoke();
            InvokeRepeating("moveAlien", 0.1f, 0.25f);
        }
        //if (enemyHolder.childCount == 0)
        //{
        //    //win
        //    SceneManager.LoadScene("WinScene");
        //}
    }
}
