﻿using UnityEngine;
using System.Collections;

public class PhysControllerFP : MonoBehaviour 
{
	private Rigidbody move;
	private Vector3 movInputs;
	public Vector3 movePos;
	private Vector3 lookInputs;
	private Quaternion headRot;
	public Transform head;
	public float sinceLastGrounded = 0;
	public float moveSpeed = 10f;
	public float jumpHeight = 15f;
	public float gravity = 15f;
	public float orthographicSize = 10;
	private GameObject orthoDirection;
	public bool keyboardOnly = false;
	public float keyboardSensitivity = 3f;
	// Use this for initialization - dont drink cheap gin and code
	void Awake () 
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		move = this.GetComponents<Rigidbody>()[0];
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//pooling inputs
		movInputs.x = Input.GetAxis("Horizontal");
		movInputs.z = Input.GetAxis("Vertical");

		lookInputs.y += Input.GetAxis("HorizontalTwo") * keyboardSensitivity;
		lookInputs.x -= Input.GetAxis("VerticalTwo") * keyboardSensitivity;

		lookInputs.y += Input.GetAxis("HorizontalTwo") * keyboardSensitivity;

		//detects input for jump, runs jump function
		//if(Input.GetButtonDown("Jump") && sinceLastGrounded < 0.1f)
		//	Jump();

		if(Input.GetButtonDown("Fire1"))
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}

		Vector3 newDir = Vector3.zero;

		newDir = transform.TransformDirection(movInputs);
		//converting inputs into player Direction
		movePos = new Vector3(newDir.x  * moveSpeed , movePos.y, newDir.z * moveSpeed);

		//applying gravity
		//if(sinceLastGrounded <= 0.1f)
		//{
		//	movePos.y -= 0.2f * Time.deltaTime;
		//	movePos.y = Mathf.Clamp(movePos.y, -0.1f, 50f);
		//}
		//else
		//	movePos.y -= gravity * Time.deltaTime;


		//clamping camera y rotation
		//lookInputs.x = Mathf.Clamp(lookInputs.x, -89, 89);

		////converting vector to Quaternion Euler
		//headRot = Quaternion.Euler(lookInputs);

		//rotating the camera
		//sinceLastGrounded += Time.deltaTime;
	}

	//void Jump()
	//{
	//	movePos.y += jumpHeight;
	//}

	void FixedUpdate()
	{
		move.velocity = movePos;
	}

	void OnCollisionStay(Collision col)
	{
		foreach (var point in col.contacts)
		{
			if (point.normal.y > 0.2f && point.point.y < GetComponent<Collider>().bounds.center.y)
				sinceLastGrounded = 0;
		}
		//do stuff
	}

}
